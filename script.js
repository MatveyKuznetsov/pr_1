// http://www.omdbapi.com/?s=batman&apikey=d5677312

const inputField = document.querySelector('.input')
const searchBtn = document.querySelector('.searchBtn')

const filmsBox = document.querySelector('.films')

const startMessage = document.querySelector('.start')
const wrongMessage = document.querySelector('.wrong')
const errorMessage = document.querySelector('.error')

const wrong = message => {
    startMessage.classList.add('hidden')
    errorMessage.classList.add('hidden')
    wrongMessage.innerHTML = message
    wrongMessage.classList.remove('hidden')
}

const error = () => {
    wrongMessage.classList.add('hidden')
    startMessage.classList.add('hidden')
    errorMessage.classList.remove('hidden')
}

const render = data => {
    
    startMessage.classList.add('hidden')
    errorMessage.classList.add('hidden')
    wrongMessage.classList.add('hidden')
    
    filmsBox.innerHTML = ''; 
    data.Search.forEach(film => {
        let HTML = `
        <div class="film">
            <div class="poster">
                <img src="${film.Poster !== 'N/A' ? film.Poster : 'img/placeholder.png'}" alt>
            </div>
            <div class="info">
                <h2 class="film-name">${film.Title}</h2>
                <p class="year">${film.Year}</p>
            </div>
        </div>
        `
        filmsBox.innerHTML += HTML
    })

}

const encodeData = data => 
    Object.keys(data).map(key => {
        return [key, data[key]].map(encodeURIComponent).join('=')
    }).join('&')

const makeRequest = queryString => {

    let querySettings = {
        s: queryString,
        apikey: 'd5677312'
    }

    let query = encodeData(querySettings)

    let baseURL = 'http://www.omdbapi.com'
    let requestURL = `${baseURL}/?${query}`
    

    
    new Promise((res, rej) => {
        let XHR = new XMLHttpRequest()
        XHR.open('GET', requestURL)
        XHR.send()
        XHR.addEventListener('load', () => {
            res(JSON.parse(XHR.responseText))
        })
        XHR.addEventListener('error', () => {
            rej(XHR.statusText)
        })
    })
    .then(data => {
        if (data.Response === 'False') wrong(data.Error)
        else render(data)
    })
    .catch(err => {
        error()
        throw new Error('Error: ' + err)
    })
}

searchBtn.addEventListener('click', e => {
    e.preventDefault()
    if (inputField.value) makeRequest(inputField.value)
})

window.addEventListener('keydown', e => {
    if (inputField.value && e.keyCode === 13) {
        e.preventDefault()
        makeRequest(inputField.value)
    }
})